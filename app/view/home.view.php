<?php
require('app/core/password.class.php');
include 'app/model/users.model.php';
include 'app/controller/usersDAO.php';
$users = new Users();
$usersDAO = new UsersDAO($db);

$password = new Password();
if (isset($_POST['users'])) {
  $form = array(
    $_POST['users'],

  );
  $emailExist = $usersDAO->getByEmail($form[0]['email']);
  if (!empty($form[0]['password']) && !empty($form[0]['name']) && !empty($form[0]['company']) && !empty($form[0]['tellphone']) && !empty($form[0]['email']) && !empty($form[0]['cnpj'])) {


    if (!$emailExist) {

      $password_hash = $password->encrypt($form[0]['password']);

      $users->setName($form[0]['name']);
      $users->setCompany($form[0]['company']);
      $users->setEmail($form[0]['email']);
      $users->setPassword($password_hash);
      $users->setTellphone($form[0]['tellphone']);
      $users->setCnpj($form[0]['cnpj']);
      $users->setActivy($form[0]['activity']);
      $users->setProducts_type($form[0]['products_type']);
      
      $usersDAO->users = $users;
      $usersDAO->insert();

      $url->redirect('home&msg=success');
    } else {
      $url->redirect('home&msg=danger');
    }
  } else {
    $url->redirect('home&msg=danger-campus');
  }
  //name company cnpj email password
}


?>


<!-- Banner
============================================= -->

<section id="banner">


  <div class="banner-parallax" data-banner-height="800">
    <img src="public/assets/images/files/parallax-bg/img-1.jpg" alt="">
    <!-- <div class="overlay-colored color-bg-white opacity-80"></div>.overlay-colored end -->
    <div class="slide-content">

      <div class="container">
        <div class="row">
          <div class="col-md-6">

            <div class="banner-center-box text-white md-text-center">
              <h1>
                Quer vender mais ?
                <br />Vem para o
                <br /> Portal da Maria.
              </h1>
              <div class="description">
                Se inscreva no maior Marketplace para sua empresa !
                O melhor parceiro que você poderá ter para o seu negócio, comprar, vender, intermediar....
                <br>Vem para o Portal da Maria !
              </div>
            </div><!-- .banner-center-box end -->

          </div><!-- .col-md-6 end -->
          <div class="col-md-6">

            <div class="cta-subscribe box-form text-center">
              <div class="box-content">
                <div class="cs-notifications">
                  <div class="cs-notifications-content"></div>
                </div><!-- .cs-notifications end -->
                <form method="POST" class="redirected">

                  <div class="form-row">
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label for="cs2Name">Nome</label>
                        <input type="text" name="users[name]" class="form-control" required>
                      </div><!-- .form-group end -->
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label for="cs2Name">Nome da Empresa</label>
                        <input type="text" name="users[company]" class="form-control" required>
                      </div><!-- .form-group end -->
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label for="cs2Name">Telefone</label>
                        <input type="text" name="users[tellphone]" class="form-control" required>
                      </div><!-- .form-group end -->
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label for="cs2Name">CNPJ</label>
                        <input type="text" name="users[cnpj]" id="cnpj" class="form-control phone_us" required>
                      </div><!-- .form-group end -->
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label for="cs2Name">Quais produtos irá vender ?</label>
                        <input type="text" name="users[products_type]" id="cnpj" class="form-control phone_us" required>
                      </div><!-- .form-group end -->
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label for="cs2Name">Qual atividade irá execer ?</label>
                        <select name="users[activity]" class="form-control">
                          <option selected disabled> Selecione uma opção</option>
                          <option value="Lojista">Lojista</option>
                          <option value="Representante">Representante</option>
                        </select>
                      </div><!-- .form-group end -->
                    </div>
                    <div class="col-lg-12">
                      <div class="form-group">
                        <label for="cs2Email">Email</label>
                        <input type="text" name="users[email]" class="form-control" required>
                      </div><!-- .form-group end -->
                    </div>

                    <div class="col-lg-12">
                      <div class="form-group">
                        <label for="cs2PhoneNum">Senha</label>
                        <input type="password" name="users[password]"  class="form-control" required>
                      </div><!-- .form-group end -->
                    </div>
                    <div class="col-lg-12">
                      <?php
                      if (!empty($_GET['msg']) && $_GET['msg'] == 'success') { ?>
                        <div class="alert-message-create-success text-center" style="margin-bottom: 10px;">
                          <span><b>Cadastro efetuado com sucesso</b></span>
                        </div>
                      <?php } ?>
                      <?php
                      if (!empty($_GET['msg']) && $_GET['msg'] == 'danger') { ?>
                        <div class="alert-message-create-danger text-center" style="margin-bottom: 10px;">
                          <span><b>Email já cadastrado, espere por novidades</b></span>
                        </div>
                      <?php } ?>
                      <?php
                      if (!empty($_GET['msg']) && $_GET['msg'] == 'danger-campus') { ?>
                        <div class="alert-message-create-danger text-center" style="margin-bottom: 10px;">
                          <span><b>Por favor, preencha todos os campos</b></span>
                        </div>
                      <?php } ?>



                    </div>
                    <div class="col-lg-12">
                      <div class="form-group">
                        <input type="submit" class="form-control" value="CADASTRAR">
                      </div><!-- .form-group end -->
                    </div>
                  </div>
                </form><!-- #form-cta-subscribe-2 end -->
              </div><!-- .box-content end -->
            </div><!-- .box-form end -->

            <!-- <div class="banner-center-box md-text-center">
              <img class="mt-md-30" src="images/files/illustrator-featured-img-1.png" alt="Featured Image">
            </div> -->

          </div><!-- .col-md-6 end -->
        </div><!-- .row end -->
      </div><!-- .container end -->

    </div><!-- .slide-content end -->
  </div><!-- .banner-parallax end -->

</section><!-- #banner end -->

<!-- Content
============================================= -->
<section id="content">

  <div id="content-wrap">

    <!-- === Intro Features =========== -->
    <div id="intro-features" class="flat-section">

      <div class="section-content">

        <div class="container">
          <div class="row">
            <hr class="divider-line space-100">

            <div class="col-md-6">

              <div class="section-title">

                <h2>Você pode ser nosso parceiro das seguintes formas</h2>
                <!-- <p>
                  This should be used to tell a story and let your users know a little more about your product or service. How
                  can you benefit them?
                </p> -->
              </div><!-- .section-title end -->

              <div class="row">
                <div class="col-sm-6">

                  <div class="box-info box-info-1">
                    <div class="box-icon icon">
                      <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                    </div>
                    <div class="box-content">
                      <h4>Representantes</h4>

                    </div><!-- .box-content end -->
                  </div><!-- .box-info end -->

                </div><!-- .col-sm-6 end -->
                <div class="col-sm-6">

                  <div class="box-info box-info-1">
                    <div class="box-icon icon">
                      <i class="fa fa-users" aria-hidden="true"></i>
                    </div>
                    <div class="box-content">
                      <h4>Afiliados</h4>

                    </div><!-- .box-content end -->
                  </div><!-- .box-info end -->

                </div><!-- .col-sm-6 end -->
                <div class="col-sm-6" style="padding-top: 20px;">

                  <div class="box-info box-info-1 mb-sm-40">
                    <div class="box-icon icon">
                      <i class="fa fa-rocket" aria-hidden="true"></i>
                    </div>
                    <div class="box-content">
                      <h4>Lojas</h4>

                    </div><!-- .box-content end -->
                  </div><!-- .box-info end -->

                </div><!-- .col-sm-6 end -->

              </div><!-- .row end -->
              <div class="row">

              </div>
            </div><!-- .col-md-6 end -->
            <div class="col-md-6 text-right md-text-left">
              <img class="mt-md-60" src="public/assets/images/files/illustrator-featured-img-2.png" alt="">
            </div><!-- .col-md-6 end -->

          </div><!-- .row end -->
        </div><!-- .container end -->

      </div><!-- .section-content end -->

    </div><!-- .flat-section end -->

  </div><!-- #content-wrap -->

</section><!-- #content end -->

<script>
  $(document).ready(function() {
    $('#council_number').mask('0000 0000-0');
    $('#date_time').mask('00/00/0000 00:00:00');
    $('#cnpj').mask('00.000.000/0000-00')
    $('#rg').mask('0000000000-0');
    $('.agency').mask('0000000');
    $('.account').mask('000000000000');
    $('#postcode').mask('00000-000');
    $('#phone_residential').mask('(00) 0000-0000');
    $('#phone_commercial').mask('(00) 0000-0000');
    $('#phone_us').mask('(00) 0 0000-0000');
    $('#cpf').mask('000.000.000-00')

  });
</script>