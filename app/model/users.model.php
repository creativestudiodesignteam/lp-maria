<?php 

class Users {

    private $id;
    private $name;
    private $company;
    private $cnpj;
    private $email;
    private $tellphone;
    private $products_type;
    private $activy;
    private $password;
    private $create_at;
    private $update_at;

    public function getId() {
        return $this->id; 
    }

    public function getName() {
        return $this->name; 
    }

    public function getCompany() {
        return $this->company; 
    }

    public function getCnpj() {
        return $this->cnpj; 
    }

    public function getEmail() {
        return $this->email; 
    }

    public function getTellphone() {
        return $this->tellphone; 
    }
    public function getProducts_type() {
        return $this->products_type; 
    }
    public function getActivy() {
        return $this->activy; 
    }
    public function getPassword() {
        return $this->password; 
    }

    public function getCreate_at() {
        return $this->create_at; 
    }
    
    public function getUpdate_at() {
        return $this->update_at; 
    }

    public function setId($id) {
        $this->id = $id; 
    }

    public function setName($name) {
        $this->name = $name; 
    }

    public function setCompany($company) {
        $this->company = $company; 
    }

    public function setCnpj($cnpj) {
        $this->cnpj = $cnpj; 
    }

    public function setEmail($email) {
        $this->email = $email; 
    }

    public function setTellphone($tellphone) {
        $this->tellphone = $tellphone; 
    }
    public function setProducts_type($products_type) {
        $this->products_type = $products_type; 
    }
    public function setActivy($activy) {
        $this->activy = $activy; 
    }

    public function setPassword($password) {
        $this->password = $password; 
    }

    public function setCreate_at($create_at) {
        $this->create_at = $create_at; 
    }

    public function setUpdate_at($update_at) {
        $this->update_at = $update_at; 
    }
}