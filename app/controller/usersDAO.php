<?php

class UsersDAO
{

    public $users;
    private $db;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function insert()
    {
        $insert = $this->db->prepare("INSERT INTO `users` (name, company, cnpj, email, tellphone,products_type,	activy, password) VALUES (:name, :company, :cnpj, :email, :tellphone,:products_type, :activy, :password)");

        $insert->bindValue(":name", $this->users->getName());
        $insert->bindValue(":company", $this->users->getCompany());
        $insert->bindValue(":cnpj", $this->users->getCnpj());
        $insert->bindValue(":email", $this->users->getEmail());
        $insert->bindValue(":tellphone", $this->users->getTellphone());
        $insert->bindValue(":products_type", $this->users->getProducts_type());
        $insert->bindValue(":activy", $this->users->getActivy());
        $insert->bindValue(":password", $this->users->getPassword());

        $insert->execute();
    }
    public function getById($id)
    {
        $list = $this->db->prepare("SELECT * FROM `users` WHERE `id`='$id'");
        $list->execute();
        return $list->fetch(PDO::FETCH_ASSOC);
    }
    public function getByEmail($email)
    {
        $list = $this->db->prepare("SELECT * FROM `users` WHERE `email`='$email'");
        $list->execute();
        return $list->fetch(PDO::FETCH_ASSOC);
    }

    public function listAll()
    {
        $list = $this->db->prepare("SELECT * FROM `users`");
        $list->execute();

        return $list->fetchAll(PDO::FETCH_ASSOC);
    }

    public function delete()
    {
        $delete = $this->db->prepare("DELETE FROM `users` WHERE `id` = :id");
        $delete->bindValue(":id", $this->users->getId());

        $delete->execute();
    }

    public function update()
    {
        $update = $this->db->prepare("UPDATE  `users` SET name = :name, company = :company, cnpj = :cnpj, email = :email, tellphone = :tellphone, password = :password, create_at = :create_at, update_at = :update_at WHERE id = :id");

        $update->bindValue(":id", $this->users->getId());
        $update->bindValue(":name", $this->users->getName());
        $update->bindValue(":company", $this->users->getCompany());
        $update->bindValue(":cnpj", $this->users->getCnpj());
        $update->bindValue(":email", $this->users->getEmail());
        $update->bindValue(":tellphone", $this->users->getTellphone());
        $update->bindValue(":password", $this->users->getPassword());
        $update->bindValue(":create_at", $this->users->getCreate_at());
        $update->bindValue(":update_at", $this->users->getUpdate_at());

        $update->execute();
    }
}
