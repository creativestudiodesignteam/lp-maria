<?php

class Connection
{

   /*private $data = array(
        'host' => 'localhost',
        'database' => 'landing-api.sql',
        'username' => 'root',
        'password' => '',
    );*/
    private $data = array(
        'host' => 'localhost',
        'database' => 'leads_landing',
        'username' => 'admin-landing',
        'password'  => 'admin',
    );
          /* Name,company_name,tellphone, cnpj,email, password */

    public function connect()
    {
        $pdo = new PDO(
            'mysql:dbname=' . $this->data['database'] .
                '; charset=utf8; host' . $this->data['host'],
            $this->data['username'],
            $this->data['password']
        );

        return $pdo;
    }
}
