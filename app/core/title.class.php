<?php
// Made by Guilherme Girão (https://github.com/guilhermegirao)

class Title
{

    public $titles = array(
        'error' => 'ERROR!',
        'errorpermission' => 'ERROR!',
        'home'  => 'Página Inicial',
        'sistema/login' => 'Entrar',

        'sobre' => 'Sobre a Color',
        'patrocinador' => 'Seja um Patrocinador',
        'contato' => 'Contato',
        'cidades' => 'Cidades',
        'duvidas' => 'Dúvidas - FAQ',
        'about' => 'Sobre a Color',
        'partners' => 'Seja um Patrocinador',
        'contact' => 'Contato',


        'sistema/home' => 'Dashboard',
        'sistema/pages/events/index' => 'Gerenciar Eventos',
        'sistema/pages/events/create' => 'Cadastrar Eventos',
        'sistema/pages/events/edit' => 'Editar Eventos',

        'sistema/pages/kits/index' => 'Gerenciar Kits',
        'sistema/pages/kits/create' => 'Cadastrar Kits',
        'sistema/pages/kits/edit' => 'Editar Kits',

        'sistema/pages/emails/index' => 'Gerenciar Kits',

    );

    public function pageTitle($key)
    {
        if (array_key_exists($key, $this->titles)) {
            return $this->titles[$key];
        } else {
            return $this->titles['error'];
        }
    }
}
