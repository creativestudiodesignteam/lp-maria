<?php

class Notification
{

    public function notify($type, $msg)
    {
        if (!empty($msg)) {

            if ($type == "danger") {
                $icon = 'down';
            } elseif ($type == "success") {
                $icon = 'up';
            }
            $body = "<div class='alert round bg-" . $type . " alert-icon-left alert-dismissible mb-2'
            role='alert'>
            <span class='alert-icon'>
                <i class='ft-thumbs-" . $icon . "'></i>
            </span>
            <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
            </button>
            <span>" . $msg . "</span>
            </div>";

            echo ($body);
        }
    }
}
