		<!-- Footer
		============================================= -->
		<footer id="footer">
			<div id="footer-bar-2" class="footer-bar">

				<div class="footer-bar-wrap">

					<div class="container">
						<div class="row">
							<div class="col-md-12">

								<div class="fb-row">
									<div class="copyrights-message">2020 © <a href="creativedd.com.br/" target="_blank"><span class="colored">Creative dev & design</span></a>. All Rights Reserved.</div>
									<ul class="social-icons animated x4 grey hover-colorful icon-only">
										<li><a class="si-facebook" href="https://www.facebook.com/PortaldaMaria/"><i class="fa fa-facebook"></i><i class="fa fa-facebook"></i></a></li>
										<li><a class="si-instagramorange" href="https://www.instagram.com/portaldamaria_/?hl=pt-br"><i class="fa fa-instagram"></i><i class="fa fa-instagram"></i></a></li>
										<li><a class="si-twitter" href="https://www.youtube.com/channel/UCJSj2gLwsua3PZOADArFmMA"><i class="fa fa-youtube"></i><i class="fa fa-youtube"></i></a></li>
										<li><a class="si-twitter" href="https://www.linkedin.com/in/portal-da-maria-marketplace-880100186/"><i class="fa fa-linkedin"></i><i class="fa fa-linkedin"></i></a></li>

									</ul><!-- .social-icons end -->
								</div><!-- .fb-row end -->

							</div><!-- .col-md-12 end -->
						</div><!-- .row end -->
					</div><!-- .container end -->

				</div><!-- .footer-bar-wrap -->

			</div><!-- #footer-bar-2 end -->

		</footer><!-- #footer end -->

		</div><!-- #full-container end -->

		<a class="scroll-top-icon scroll-top" href="#"><i class="fa fa-angle-up"></i></a>

		<!-- External JavaScripts
	============================================= -->

		<script src="public/assets/js/jquery.js"></script>
		<script src="public/assets/js/jRespond.min.js"></script>
		<script src="public/assets/js/jquery.easing.min.js"></script>
		<script src="public/assets/js/jquery.waypoints.min.js"></script>
		<script src="public/assets/js/jquery.fitvids.js"></script>
		<script src="public/assets/js/jquery.stellar.js"></script>
		<script src="public/assets/js/owl.carousel.min.js"></script>
		<script src="public/assets/js/jquery.mb.YTPlayer.min.js"></script>
		<script src="public/assets/js/jquery.magnific-popup.min.js"></script>
		<script src="public/assets/js/jquery.ajaxchimp.min.js"></script>
		<script src="public/assets/js/jquery.validate.min.js"></script>

		<script src="public/assets/js/simple-scrollbar.min.js"></script>
		<script src='public/assets/js/functions.js'></script>
		<script src="public/assets/js/jquerymask.min.js"></script>

		</body>

		</html>