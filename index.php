<?php

include 'app/core/connection.class.php';
include 'app/core/session.class.php';
include 'app/core/title.class.php';
include 'app/core/url.class.php';
include 'app/core/notify.class.php';
include 'app/core/components.class.php';

$db         = new Connection();
$url        = new URL();
$components = new Components();

$db = $db->connect();


$components->header();


foreach ($_GET as $key => $value) if (!is_array($value)) $_GET[$key] = $url->filter($value);
foreach ($_POST as $key => $value) if (!is_array($value)) $_POST[$key] = $url->filter($value);
?>

<?php

$url->page();

?>

<?php
$components->footer();

?>
