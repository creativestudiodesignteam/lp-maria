<?php

require_once ('app/core/connection.class.php');
$db         = new Connection();
$db = $db->connect();

$dir = 'app/controller';
$column = 'users';
$sql ="SHOW COLUMNS FROM `$column`";

echo 'Tabela: '.$column;

$listar = $db->prepare($sql);
$listar->execute();

$arquivo = fopen($dir.'/'.$column.'DAO.php', 'w');

fwrite($arquivo, '<?php '.PHP_EOL.PHP_EOL.'class '.ucfirst($column).'DAO {'.PHP_EOL.PHP_EOL);

fwrite($arquivo, '    public $'.$column.';'.PHP_EOL);
fwrite($arquivo, '    private $db;'.PHP_EOL);

fwrite($arquivo, PHP_EOL);

$constructor = '    public function __construct ($db) {'.PHP_EOL.'        $this->db = $db;'.PHP_EOL.'    }'.PHP_EOL.PHP_EOL;

fwrite($arquivo, $constructor);

//Insert

$insert = '    public function insert () {'.PHP_EOL.'        $insert = $this->db->prepare("INSERT INTO `'.$column.'` (';
$i = 0;

$listar = $db->prepare($sql);
$listar->execute();
$num_row = $listar->rowCount();

foreach($listar as $l) {
    $i++;
    $row = $l['Field'];

    if ($row == 'id' || $row == 'cod') continue;
    

    if ($i != $num_row) {
        $insert .= $row.', ';
    } else {
        $insert .= $row;
        $insert .= ') VALUES (';

        $listar = $db->prepare($sql);
        $listar->execute();
        $i = 0;
        
        foreach($listar as $l) {
            $i++;
            $row = $l['Field'];

            if ($row == 'id' || $row == 'cod') continue;

            if ($i != $num_row) {
                $insert .= ':'.$row.', ';
            } else {
                $insert .= ':'.$row;
                $insert .= ')");'.PHP_EOL.PHP_EOL;

                $listar = $db->prepare($sql);
                $listar->execute();
                $i = 0;
                
                //VARIAVEIS
                foreach($listar as $l) {
                    $i++;
                    $row = $l['Field'];

                    if ($row == 'id' || $row == 'cod') continue;

                    $insert .= '        $insert->bindValue(":'.$row.'", $this->'.$column.'->get'.ucfirst($row).'());'.PHP_EOL;

                    if ($i == $num_row) {
                        $insert .= PHP_EOL.'        $insert->execute();';
                        $insert .= PHP_EOL.'    }';
                        $insert .= PHP_EOL.PHP_EOL;
                    }
                }
            }
        }
    }
}

fwrite($arquivo, $insert);

//List All
$listAll = '    public function listAll () {'.PHP_EOL.'        $list = $this->db->prepare("SELECT * FROM `'.$column.'`");'.PHP_EOL.'        $list->execute();'.PHP_EOL.'        '.PHP_EOL.'        return $list->fetchAll(PDO::FETCH_ASSOC);'.PHP_EOL.'    }';
$listAll .= PHP_EOL.PHP_EOL;

fwrite($arquivo, $listAll);

//Delete

$listar = $db->prepare($sql);
$listar->execute();
$i = 0;
$id = '';

foreach($listar as $l) {
    if (!$i) {
        $id = $l['Field'];
        $i++;
    } else {
        break;
    }
}

$delete = '    public function delete () {'.PHP_EOL.'        $delete = $this->db->prepare("DELETE FROM `'.$column.'` WHERE `'.$id.'` = :'.$id.'");'.PHP_EOL.'        $delete->bindValue(":'.$id.'", $this->'.$column.'->get'.ucfirst($id).'());'.PHP_EOL.'        '.PHP_EOL.'        $delete->execute();'.PHP_EOL.'    }';
$delete .= PHP_EOL.PHP_EOL;

fwrite($arquivo, $delete);

//Update

$update = '    public function update () {'.PHP_EOL.'        $update = $this->db->prepare("UPDATE  `'.$column.'` SET ';
$i = 0;
$lock = 0;
$lock_id = 0;

$listar = $db->prepare($sql);
$listar->execute();
$num_row = $listar->rowCount();

foreach($listar as $l) {
    $i++;
    $row = $l['Field'];

    if ($row == 'id' || $row == 'cod') continue;
    
    if ($i != $num_row) {
        $update .= $row.' = :'.$row.', ';
    } else {
        $update .= $row.' = :'.$row;
        $update .= ' WHERE '.$id.' = :'.$id.'");';    
        $update .= PHP_EOL.PHP_EOL;

        $listar = $db->prepare($sql);
        $listar->execute();
        $i = 0;
        
        //VARIAVEIS
        foreach($listar as $l) {
            $i++;
            $row = $l['Field'];

            if (!$lock_id) {
                $update .= '        $update->bindValue(":'.$id.'", $this->'.$column.'->get'.ucfirst($id).'());'.PHP_EOL;
                $lock_id = 1;
            }

            if ($row == 'id' || $row == 'cod') {
                if ($lock_id) continue;
            }

            $update .= '        $update->bindValue(":'.$row.'", $this->'.$column.'->get'.ucfirst($row).'());'.PHP_EOL;

            if ($i == $num_row) {
                $update .= PHP_EOL.'        $update->execute();';
                $update .= PHP_EOL.'    }';
                $update .= PHP_EOL.PHP_EOL;
            }
        }
    }
}

fwrite($arquivo, $update);

//Final
fwrite($arquivo, PHP_EOL.'}');
fclose($arquivo);

echo '<br>Sucesso!';