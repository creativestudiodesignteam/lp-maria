## Documentação

### Iniciar projeto no localhost

```
php -S localhost:8000
```

### Instalar packages npm

```
npm install ou yarn
```

### Sass compiler

```
npm run watch-css
```

### Fluxo para Feature

Na branch:

git stash clear

git add .

git stash

(Guarda na memória suas modificações)

git checkout branch/develop

(Altera para branch develop)

git pull

(Sincroniza a branch da sprint local)

git checkout avansys/feature

git rebase branch/develop

(Traz as modificações da sprint para sua FEATURE)

git stash apply

(Aplica as modificações previamente guardadas)

#### ps: outra opção é usar git stash pop, porém esse aplica as modificações já limpando o histórico de stash, impossibilitando vc aplicar novamente no caso de algum erro)

### Na branch:

git add .

(Adiciona todas as configurações no git)

SEMPRE verifique se é realmente tudo o que está lá que você quer subir, pelo próprio vscode você pode reverter o que não quer

git commit –m “[sua branch] o que você fez”

(Faz commit das alterações)

git push

(Sobe para o origin)

git checkout branch/develop

git merge --no-ff branch/feature

(Realiza o merge da sua branch com a develop)

git push
