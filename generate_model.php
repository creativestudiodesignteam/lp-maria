<?php

require_once ('app/core/connection.class.php');
$db         = new Connection();
$db = $db->connect();

$dir = 'app/model';
$column = 'users';
$sql ="SHOW COLUMNS FROM `$column`";

echo 'Tabela: '.$column;

$listar = $db->prepare($sql);
$listar->execute();

$arquivo = fopen($dir.'/'.$column.'.model.php', 'w');

fwrite($arquivo, '<?php '.PHP_EOL.PHP_EOL.'class '.ucfirst($column).' {'.PHP_EOL.PHP_EOL);

foreach($listar as $l) {
    $row = $l['Field'];
    $var = '    private $'.$row.';'.PHP_EOL;

    fwrite($arquivo, $var);
}

fwrite($arquivo, PHP_EOL);

$listar = $db->prepare($sql);
$listar->execute();

foreach($listar as $l) {
    $row = $l['Field'];
    $get = '    public function get'.ucfirst($row).'() {'.PHP_EOL.'        return $this->'.$row.'; '.PHP_EOL.'    }'.PHP_EOL.PHP_EOL;

    fwrite($arquivo, $get);
}

$listar = $db->prepare($sql);
$listar->execute();

foreach($listar as $l) {
    $row = $l['Field'];
    $set = '    public function set'.ucfirst($row).'($'.$row.') {'.PHP_EOL.'        $this->'.$row.' = $'.$row.'; '.PHP_EOL.'    }'.PHP_EOL.PHP_EOL;

    fwrite($arquivo, $set);
}

fwrite($arquivo, PHP_EOL.'}');
fclose($arquivo);

echo '<br>Sucesso!';